# Escribe otro programa que pida una lista de números como la anterior y al final muestre por pantalla el máximo
# y mínimo de los números, en vez de la media.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
lista_numérica= []
cant= int (input("Ingrese la cantidad de números: "))
máximo=0
mínimo=0
con=1
while (cant > 0):
    try:
        número= float (input("Introduzca un número " + str(con) + ":"))
        lista_numérica.append(número)
        con= con + 1
        cant= cant - 1
    except:
        print ("Entrada inválida")
máximo= max(lista_numérica)
mínimo= min(lista_numérica)
print ("Lista: ", lista_numérica)
print ("El número máximo es: ", máximo)
print ("El número mínimo es: ", mínimo)
