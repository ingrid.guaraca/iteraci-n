#Escribe un programa que lea repetidamente números hasta que el usuario introduzca “fin”.
# Una vez se haya introducido “fin”, muestra por pantalla el total, la cantidad de números y la media de esos números.
# Si el usuario introduce cualquier otra cosa que no sea un número, detecta su fallo usando try y except, muestra un
# mensaje de error y pasa al número siguiente.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
con = 0
total = 0
while True:
    núm = input("Introduzca un número: ")
    try:
        total = total + int(núm)
        con= con + 1
    except:
        if núm in "fin":
            break
        print("Entrada inválida")
print(total, con, total/con)


